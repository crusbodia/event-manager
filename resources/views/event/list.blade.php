@extends('layouts.app')

@section('title', 'Events list')

@section('content')
    <div class="events">
        @foreach ($events as $event)
            <div class="event-item">
                <div class="event-title">{{ $event->name }}</div>
                <div class="event-date">{{ $event->start_date }}</div>
            </div>
        @endforeach
    </div>
@endsection