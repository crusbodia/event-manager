<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\Event;

/**
 * Class EventController
 *
 * @package App\Http\Controllers
 */
class EventController extends Controller
{
	public function index()
	{
		return view('event.list', [
			'events' => Event::query()->orderBy('start_date')->get()
		]);
	}
}