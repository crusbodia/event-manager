<?php
declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BloggerEvent extends Model
{
	/**
	 * @var string
	 */
	protected $table = 'blogger_event';

	/**
	 * @var array
	 */
	protected $casts = [
		'blogger_id' => 'int',
		'event_id' => 'int',
		'participant_number' => 'int'
	];
}