<?php
declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class Event
 * @package App\Models
 *
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon $start_date
 *
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Blogger[] $bloggers
 */
class Event extends Model
{

	/**
	 * @var array
	 */
	protected $guarded = ['id'];

	/**
	 * @var array
	 */
	protected $casts = ['id' => 'int'];

	protected $dates = ['start_date'];

	/*
	|--------------------------------------------------------------------------
	|  Relations
	|--------------------------------------------------------------------------
	*/

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function bloggers(): BelongsToMany
	{
		return $this->belongsToMany(Blogger::class);
	}
}