<?php
declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class Blogger
 * @package App\Models
 *
 * @property int $id
 * @property string $name
 * @property string $avatar
 *
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Event[] $events
 */
class Blogger extends Model
{

	/**
	 * @var array
	 */
	protected $guarded = ['id'];

	/**
	 * @var array
	 */
	protected $casts = ['id' => 'int'];

	/*
	|--------------------------------------------------------------------------
	|  Relations
	|--------------------------------------------------------------------------
	*/

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function events(): BelongsToMany
	{
		return $this->belongsToMany(Event::class);
	}
}