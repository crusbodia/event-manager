<?php
declare(strict_types=1);

use App\Models\Blogger;
use Illuminate\Database\Seeder;

/**
 * Class BloggerTableSeeder
 */
class BloggerTableSeeder extends Seeder
{

	/**
	 * @var array
	 */
	private $bloggers = [
		[
			'id' => 1,
			'name' => 'Albert Einstein',
			'avatar' => 'user1.jpeg'
		],
		[
			'id' => 2,
			'name' => 'Harry Potter',
			'avatar' => 'user2.png'
		],
		[
			'id' => 3,
			'name' => 'Spider Man',
			'avatar' => 'user3.png'
		],
		[
			'id' => 4,
			'name' => 'George Bush',
			'avatar' => 'user4.png'
		],
		[
			'id' => 5,
			'name' => 'Steve Jobs',
			'avatar' => 'user5.png'
		],
		[
			'id' => 6,
			'name' => 'Mermaid',
			'avatar' => 'user6.png'
		],
		[
			'id' => 7,
			'name' => 'Genghis Khan',
			'avatar' => 'user7.jpg'
		],
	];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->bloggers as $item) {

        	/** @var Blogger $blogger */
	        $blogger = Blogger::query()->firstOrNew([
	        	'id' => $item['id'],
	        ]);
	        $blogger->name = $item['name'];
	        $blogger->avatar = $item['avatar'];

	        $blogger->save();
        }
    }
}
