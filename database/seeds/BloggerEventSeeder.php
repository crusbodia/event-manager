<?php
declare(strict_types=1);

use App\Models\BloggerEvent;
use Illuminate\Database\Seeder;

/**
 * Class BloggerEventSeeder
 */
class BloggerEventSeeder extends Seeder
{
	/**
	 * @var array
	 */
	private $eventsParticipnats = [
		[
			'event_id' => 1,
			'bloggers' => [2, 3, 4]
		],
		[
			'event_id' => 2,
			'bloggers' => [4, 3, 1]
		],
		[
			'event_id' => 3,
			'bloggers' => [4, 7, 6, 5, 3]
		],
		[
			'event_id' => 4,
			'bloggers' => [2, 7, 1, 4, 3, 6, 5]
		],
	];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->eventsParticipnats as $eventItem) {
        	$participantNumber = 1;

        	foreach ($eventItem['bloggers'] as $blogger) {
        		BloggerEvent::query()->create([
        			'blogger_id' => $blogger,
			        'event_id' => $eventItem['event_id'],
			        'participant_number' => $participantNumber++
		        ]);
	        }
        }
    }
}
