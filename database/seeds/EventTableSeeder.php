<?php
declare(strict_types=1);

use App\Models\Event;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

/**
 * Class EventTableSeeder
 */
class EventTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    $events = [
		    [
		    	'id' => 1,
			    'name' => 'Revolution',
			    'start_date' => Carbon::today()->timestamp
		    ],
		    [
			    'id' => 2,
			    'name' => 'Sleep all day',
			    'start_date' => Carbon::tomorrow()->timestamp
		    ],
		    [
			    'id' => 3,
			    'name' => 'Apocalypse',
			    'start_date' => Carbon::now()->addMonths(2)->timestamp
		    ],
		    [
			    'id' => 4,
			    'name' => 'New Year',
			    'start_date' => Carbon::now()->addMonths(1)->timestamp
		    ]
	    ];

	    foreach ($events as $item) {
		    /** @var Event $event */
	    	$event = Event::query()->firstOrNew([
		    	'id' => $item['id']
		    ]);
		    $event->name = $item['name'];
		    $event->start_date = $item['start_date'];

		    $event->save();
	    }
    }
}
