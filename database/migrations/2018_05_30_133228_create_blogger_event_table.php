<?php
declare(strict_types=1);

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateBloggerEventTable
 */
class CreateBloggerEventTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blogger_event', function (Blueprint $table) {
	        $table->unsignedInteger('blogger_id');
	        $table->unsignedInteger('event_id');
	        $table->unsignedInteger('participant_number');

	        $table->unique(['blogger_id', 'event_id', 'participant_number'], 'blogger_id_event_id_participant_number');

	        $table->foreign('blogger_id')
	              ->references('id')
	              ->on('bloggers')
	              ->onUpdate('cascade')
	              ->onDelete('cascade');

	        $table->foreign('event_id')
	              ->references('id')
	              ->on('events')
	              ->onUpdate('cascade')
	              ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blogger_event');
    }
}
